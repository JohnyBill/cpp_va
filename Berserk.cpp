#include "Berserk.hh"

Berserk::Berserk(int x, int y) : Unit(x, y)
{
  this->setHP(15);
  this->setAttack(4);
  this->setSpeed(5);
}

Berserk::~Berserk()
{
}

unitType	Berserk::getType() const
{
  return (BERSERK);
}

void		Berserk::restoreSpeed()
{
  this->setSpeed(5);
}

#ifndef BERSERK_HH_
# define BERSERK_HH_

# include "Unit.hh"

class Berserk : public Unit
{
public:
  Berserk(int, int);
  ~Berserk();

  void		restoreSpeed();
  unitType	getType() const;
};

#endif

#include "Case.hh"

Case::Case(fieldType ft, Unit *unit)
{
  this->_unit = unit;
  this->_fieldType = ft;
}

Case::Case(fieldType ft)
{
  this->_fieldType = ft;
}

Case::~Case()
{
}

fieldType	Case::getFieldType() const
{
  return (this->_fieldType);
}

Unit		*Case::getUnit() const
{
  return (this->_unit);
}

void		Case::setUnit(Unit* unit)
{
  this->_unit = unit;
}

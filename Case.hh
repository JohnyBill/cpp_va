#ifndef CASE_HH_
# define CASE_HH_

# include <string>
# include <iostream>

# include "Unit.hh"

enum fieldType
  {
    EMPTY = 0,
    MOUNTAIN = 1,
    RIVER = 2,
    PLAIN = 3
  };

class Case
{
  Unit		*_unit;
  fieldType	_fieldType;

public:
  Case(fieldType, Unit*);
  Case(fieldType);
  ~Case();

  fieldType	getFieldType() const;
  Unit		*getUnit() const;
  void		setUnit(Unit*);
};

#endif

#include "Display.hh"

Display::Display(){}

Display::~Display() {}

void	Display::init(int width, int height, const std::string &windowName)
{
  this->_window = new sf::RenderWindow(sf::VideoMode(width, height, 32), windowName);

  sf::Image icon;

  if(!icon.loadFromFile("sprites/icone.png"))
    throw std::runtime_error("Display: can't load the icone.png");
  this->_window->setIcon(50,50,icon.getPixelsPtr());
  if (!this->_font.loadFromFile("./sprites/font.ttf"))
    throw std::runtime_error("Display: Can't load font.ttf.");
  this->load("./sprites/mountain.png");
  this->load("./sprites/water.png");
  this->load("./sprites/plain.png");
  this->load("./sprites/tank_red.png");
  this->load("./sprites/berserk_blue.png");
  this->load("./sprites/lancemissile_red.png");
  this->load("./sprites/tank_blue.png");
  this->load("./sprites/berserk_red.png");
  this->load("./sprites/lancemissile_blue.png");
}

bool	Display::ifKeyEvent()
{
  return (this->_window->pollEvent(this->_event));
}

bool	Display::ifKeyUp() const
{
  return (sf::Keyboard::isKeyPressed(sf::Keyboard::Up));
}

bool	Display::ifKeyDown() const
{
  return (sf::Keyboard::isKeyPressed(sf::Keyboard::Down));
}

bool	Display::ifKeyRight() const
{
  return (sf::Keyboard::isKeyPressed(sf::Keyboard::Right));
}

bool	Display::ifKeyLeft() const
{
  return (sf::Keyboard::isKeyPressed(sf::Keyboard::Left));
}

bool	Display::ifKeySpace() const
{
  return (sf::Keyboard::isKeyPressed(sf::Keyboard::Space));
}

bool	Display::ifKeyEscape() const
{
  return ((sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || this->_event.type == sf::Event::Closed));
}

void		Display::drawMap(const std::deque<std::deque<Case*> > &cases)
{
  int		i = 0, j = 0;
  sf::Sprite	mnt;
  sf::Sprite	wat;
  sf::Sprite	pla;

  this->_window->clear();
  mnt.setTexture(this->_textures["./sprites/mountain.png"]);
  wat.setTexture(this->_textures["./sprites/water.png"]);
  pla.setTexture(this->_textures["./sprites/plain.png"]);
  pla.setScale(2, 2);
  mnt.setScale(2, 2);
  wat.setScale(2, 2);
  while (!cases[i].empty())
    {
      j = 0;
      while (cases[i][j] != NULL && cases[i][j]->getFieldType() != EMPTY)
  	{
	  if (cases[i][j]->getFieldType() == 1)
	    {
	      mnt.setPosition(j * 64, i * 64);
	      this->_window->draw(mnt);
	      if (cases[i][j]->getUnit() != NULL && cases[i][j]->getUnit()->getTeam() == 0)
		this->displayRedUnits(cases[i][j]->getUnit()->getType(), i, j);
	      else if (cases[i][j]->getUnit() != NULL)
		this->displayBlueUnits(cases[i][j]->getUnit()->getType(), i, j);
	    }
	  else if (cases[i][j]->getFieldType() == 2)
	    {
	      wat.setPosition(j * 64, i * 64);
	      this->_window->draw(wat);
	      if (cases[i][j]->getUnit() != NULL && cases[i][j]->getUnit()->getTeam() == 0)
		this->displayRedUnits(cases[i][j]->getUnit()->getType(), i, j);
	      else if (cases[i][j]->getUnit() != NULL)
		this->displayBlueUnits(cases[i][j]->getUnit()->getType(), i, j);
	    }
	  else if (cases[i][j]->getFieldType() == 3)
	    {
	      pla.setPosition(j * 64, i * 64);
	      this->_window->draw(pla);
	      if (cases[i][j]->getUnit() != NULL && cases[i][j]->getUnit()->getTeam() == 0)
		this->displayRedUnits(cases[i][j]->getUnit()->getType(), i, j);
	      else if (cases[i][j]->getUnit() != NULL)
		this->displayBlueUnits(cases[i][j]->getUnit()->getType(), i, j);
	    }
	  ++j;
  	}
      ++i;
    }
  this->_window->display();
}

void	Display::displayRedUnits(const unitType &unit, int i, int j)
{
  sf::Sprite	redtank;
  sf::Sprite	redbrsrk;
  sf::Sprite	redlncmssl;

  redtank.setTexture(this->_textures["./sprites/tank_red.png"]);
  redbrsrk.setTexture(this->_textures["./sprites/berserk_red.png"]);
  redlncmssl.setTexture(this->_textures["./sprites/lancemissile_red.png"]);
  if (unit == TANK)
    {
      redtank.setPosition((j * 64 + 2), (i * 64 + 2));
      this->_window->draw(redtank);
    }
  else if (unit == BERSERK)
    {
      redbrsrk.setPosition((j * 64 + 2), (i * 64 + 2));
      this->_window->draw(redbrsrk);
    }
  else if (unit == LANCEMISSILE)
    {
      redlncmssl.setPosition((j * 64 + 2), (i * 64 + 2));
      this->_window->draw(redlncmssl);
    }
}

void	Display::displayBlueUnits(const unitType &unit, int i, int j)
{
  sf::Sprite	bluetank;
  sf::Sprite	bluebrsrk;
  sf::Sprite	bluelncmssl;

  bluetank.setTexture(this->_textures["./sprites/tank_blue.png"]);
  bluebrsrk.setTexture(this->_textures["./sprites/berserk_blue.png"]);
  bluelncmssl.setTexture(this->_textures["./sprites/lancemissile_blue.png"]);
  if (unit == TANK)
    {
      bluetank.setPosition((j * 64 + 2), (i * 64 + 2));
      this->_window->draw(bluetank);
    }
  else if (unit == BERSERK)
    {
      bluebrsrk.setPosition((j * 64 + 2), (i * 64 + 2));
      this->_window->draw(bluebrsrk);
    }
  else if (unit == LANCEMISSILE)
    {
      bluelncmssl.setPosition((j * 64 + 2), (i * 64 + 2));
      this->_window->draw(bluelncmssl);
    }
}

void	Display::load(const std::string &texture)
{
  if (this->_textures.find(texture) == this->_textures.end())
    {
      this->_textures[texture];
      if (!this->_textures[texture].loadFromFile(texture))
	throw std::runtime_error("Display: can't load sprite");
    }
}

bool	Display::getMouseEvent(float *x, float *y)
{
  if(_event.type == sf::Event::MouseButtonPressed)
    {
      if (_event.mouseButton.button == sf::Mouse::Left)
	{
	  *x = _event.mouseButton.x;
	  *y = _event.mouseButton.y;
	  return (true);
	}
    }
  return (false);
}

void	Display::unload()
{
  this->_textures.clear();
}

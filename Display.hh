#ifndef DISPLAY_HH_
# define DISPLAY_HH_

# include <SFML/Graphics.hpp>

# include <iostream>
# include <stdexcept>
# include <list>
# include <string>
# include <deque>

# include "IDisplay.hh"
# include "Case.hh"

class Display : public IDisplay
{
private:
  sf::RenderWindow			*_window;
  sf::Event				_event;
  std::map<std::string, sf::Texture>	_textures;
  sf::Font				_font;

public:
  Display();
  ~Display();

  void	init(int, int, const std::string &);
  bool	ifKeyEvent();
  bool	ifKeyUp() const;
  bool	ifKeyDown() const;
  bool	ifKeyRight() const;
  bool	ifKeyLeft() const;
  bool	ifKeySpace() const;
  bool	ifKeyEscape() const;
  bool	getMouseEvent(float *, float *);
  void	load(const std::string &);
  void	unload();
  void	drawMap(const std::deque<std::deque<Case*> >&);
  void	displayRedUnits(const unitType&, int, int);
  void	displayBlueUnits(const unitType&, int, int);
};

#endif /* !DISPLAY_HH_ */

#include "Game.hh"

Game::Game()
{
  int	i = 0, j = 0;

  this->_map.resize(50);
  this->_units.resize(5);
  this->_map[i].resize(50);
  this->_display = new Display();
  this->fillMap();

  if (this->_mapY > 16 || this->_mapX > 29)
    throw std::runtime_error("The map is too bif for the screen");
    
  std::cout << "mapX : " << this->_mapX << " mapY : " << this->_mapY << std::endl;
  this->_display->init(this->_mapX * 64, this->_mapY * 64, "ShootAndRun");
  this->_display->drawMap(this->_map);
}

Game::~Game()
{
}

void	Game::run()
{
  int	i = 0;
  float k = 0, n = 0;

  while (12)
    {
      std::deque<Unit*>::iterator it;
      for (it = this->_units[i].begin(); it != this->_units[i].end(); ++it)
  	{
  	  if ((*it) != NULL)
  	    {
  	      (*it)->setTurn(true);
  	      while ((*it)->getTurn() == true && (*it)->isAlive() == true)
  		{
  		  if (this->_display->ifKeyLeft())
  		    {
  		      this->moveUnit((*it)->getX(), (*it)->getY(), 0, -1);
  		      this->_display->drawMap(this->_map);
  		      usleep(200000);
  		    }
  		  if (this->_display->ifKeyRight())
  		    {
  		      this->moveUnit((*it)->getX(), (*it)->getY(), 0, 1);
  		      this->_display->drawMap(this->_map);
  		      usleep(200000);
  		    }
  		  if (this->_display->ifKeyUp())
  		    {
  		      this->moveUnit((*it)->getX(), (*it)->getY(), -1, 0);
  		      this->_display->drawMap(this->_map);
  		      usleep(200000);
  		    }
  		  if (this->_display->ifKeyDown())
  		    {
  		      this->moveUnit((*it)->getX(), (*it)->getY(), 1, 0);
  		      this->_display->drawMap(this->_map);
  		      usleep(200000);
  		    }
  		  // if (this->_display->getMouseEvent(&k, &n))
  		  //   {
		  //     std::cout << "k : " << k << " , n : " << n << std::endl;
  		  //     usleep(200000);
  		  //   }
  		  if (this->_display->ifKeySpace())
  		    {
  		      (*it)->setTurn(false);
  		      (*it)->restoreSpeed();
  		      usleep(200000);
  		    }
  		}
  	    }
  	}
      std::cout << "switch team" << std::endl;
      (i == 0 ? i = 1 : i = 0);
    }
}

void	Game::moveUnit(const int x, const int y, const int i, const int j)
{
  if ((x + i) >= 0 && this->_map[x + i][y + j] != NULL &&
      this->_map[x + i][y + j]->getUnit() == NULL)
    {
      if (this->_map[x][y]->getFieldType() == MOUNTAIN)
        this->_map[x][y]->getUnit()->setSpeed(this->_map[x][y]->getUnit()->getSpeed() - 3);
      else if (this->_map[x][y]->getFieldType() == RIVER)
	this->_map[x][y]->getUnit()->setSpeed(this->_map[x][y]->getUnit()->getSpeed() - 2);
      else if (this->_map[x][y]->getFieldType() == PLAIN)
	this->_map[x][y]->getUnit()->setSpeed(this->_map[x][y]->getUnit()->getSpeed() - 1);
      if (this->_map[x][y]->getUnit()->getSpeed() < 0)
	{
	  std::cout << "No more speed" << std::endl;
	  return ;
	}
      this->_map[x + i][y + j]->setUnit(this->_map[x][y]->getUnit());
      this->_map[x][y]->setUnit(NULL);
      this->_map[x + i][y + j]->getUnit()->setPosX(x + i);
      this->_map[x + i][y + j]->getUnit()->setPosY(y + j);
    }
  else if ((x + i) >= 0 && this->_map[x + i][y + j] != NULL &&
      this->_map[x + i][y + j]->getUnit() != NULL && this->_map[x + i][y + j]->getUnit()->getTeam() !=
      this->_map[x][y]->getUnit()->getTeam())
    {
      this->_map[x + i][y + j]->getUnit()->setHP(this->_map[x + i][y + j]->getUnit()->getHP() - this->_map[x][y]->getUnit()->getAttack());
      if (this->_map[x + i][y + j]->getFieldType() == RIVER ||
	  this->_map[x + i][y + j]->getFieldType() == PLAIN)
	this->_map[x + i][y + j]->getUnit()->setHP(this->_map[x + i][y + j]->getUnit()->getHP() + 1);
      if (this->_map[x][y]->getFieldType() == PLAIN)
	this->_map[x + i][y + j]->getUnit()->setHP(this->_map[x + i][y + j]->getUnit()->getHP() - 1);
      if (this->_map[x + i][y + j]->getUnit()->getHP() <= 0)
	{
	  this->_map[x + i][y + j]->getUnit()->setAlive(false);
	  this->_map[x + i][y + j]->setUnit(NULL);
	}
      else
	std::cout << "vie restante : " << this->_map[x + i][y + j]->getUnit()->getHP() << std::endl;
      this->_map[x][y]->getUnit()->setTurn(false);
      this->_map[x][y]->getUnit()->restoreSpeed();
    }
}

void	Game::giveUnit(const int i, const int j, const char c)
{
  int	x;

  if (c == 49)
    {
      x = this->_maptxt.get() - 48;
      this->_map[i][j]->setUnit(new Berserk(i, j));
      this->_units[x].push_front(this->_map[i][j]->getUnit());
      this->_map[i][j]->getUnit()->setTeam(x);
    }
  else if (c == 50)
    {
      x = this->_maptxt.get() - 48;
      this->_map[i][j]->setUnit(new Tank(i, j));
      this->_units[x].push_front(this->_map[i][j]->getUnit());
      this->_map[i][j]->getUnit()->setTeam(x);
    }
  else if (c == 51)
    {
      x = this->_maptxt.get() - 48;
      this->_map[i][j]->setUnit(new LanceMissile(i, j));
      this->_units[x].push_front(this->_map[i][j]->getUnit());
      this->_map[i][j]->getUnit()->setTeam(x);
    }
  else if (c == 48)
    this->_map[i][j]->setUnit(NULL);
}

void	Game::fillMap()
{
  int	i = 0, j = 0;
  char	c;

  this->_maptxt.open("testmap.map");
  while (!this->_maptxt.eof())
    {
      c = this->_maptxt.get();
      if (c == 48)
	{
	  this->_map[i][j] = new Case(MOUNTAIN);
	  this->giveUnit(i, j, this->_maptxt.get());
	}
      else if (c == 49)
	{
	  this->_map[i][j] = new Case(RIVER);
	  this->giveUnit(i, j, this->_maptxt.get());
	}
      else if (c == 50)
	{
	  this->_map[i][j] = new Case(PLAIN);
	  this->giveUnit(i, j, this->_maptxt.get());
	}
      ++j;
      if (c == 10)
	{
	  i++;
	  this->_mapX = j - 1;
	  this->_map[i].resize(50);
	  j = 0;
	}
      else
	this->_maptxt.get();
    }
  this->_mapY = i + 1;
}

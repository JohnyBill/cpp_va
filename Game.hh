#ifndef GAME_HH_
# define GAME_HH_

# include <string>
# include <iostream>
# include <fstream>
# include <deque>
# include <iterator>
# include <unistd.h>

# include "Display.hh"
# include "Case.hh"
# include "Tank.hh"
# include "Berserk.hh"
# include "LanceMissile.hh"

class Game
{
  IDisplay				*_display;
  std::deque<std::deque<Case*> >	_map;
  std::deque<std::deque<Unit*> >	_units;
  std::ifstream				_maptxt;
  int					_mapX;
  int					_mapY;

public:
  Game();
  ~Game();

  void	run();
  void	fillMap();
  void	giveUnit(const int, const int, const char);
  void	moveUnit(const int, const int,const int,const int);
};

#endif

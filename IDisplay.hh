#ifndef IDISPLAY_HH_
# define IDISPLAY_HH_

# include <string>
# include <list>
# include <deque>

# include "Case.hh"
# include "Unit.hh"

class IDisplay
{
public:
  virtual ~IDisplay() {}

  virtual void	init(int, int, const std::string &) = 0;
  virtual bool	ifKeyEvent() = 0;
  virtual bool	ifKeyUp() const = 0;
  virtual bool	ifKeyDown() const = 0;
  virtual bool	ifKeyRight() const = 0;
  virtual bool	ifKeyLeft() const = 0;
  virtual bool	ifKeySpace() const = 0;
  virtual bool	ifKeyEscape() const = 0;
  virtual bool	getMouseEvent(float *, float *) = 0;
  virtual void	load(const std::string &) = 0;
  virtual void	unload() = 0;
  virtual void	drawMap(const std::deque<std::deque<Case*> >&) = 0;
  virtual void	displayRedUnits(const unitType&, int, int) = 0;
  virtual void	displayBlueUnits(const unitType&, int, int) = 0;
};

#endif // !IDISPLAY_HH_

#include "LanceMissile.hh"

LanceMissile::LanceMissile(int x, int y) : Unit(x, y)
{
  this->setHP(5);
  this->setAttack(6);
  this->setSpeed(6);
}

LanceMissile::~LanceMissile()
{
}

unitType	LanceMissile::getType() const
{
  return (LANCEMISSILE);
}

void	LanceMissile::restoreSpeed()
{
  this->setSpeed(6);
}

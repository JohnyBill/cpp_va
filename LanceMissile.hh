#ifndef LANCEMISSILE_HH_
# define LANCEMISSILE_HH_

# include "Unit.hh"

class LanceMissile : public Unit
{
public:
  LanceMissile(int, int);
  ~LanceMissile();

  void		restoreSpeed();
  unitType	getType() const;
};

#endif

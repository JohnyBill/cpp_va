SRC	= main.cpp \
	  Game.cpp \
	  Unit.cpp \
	  Case.cpp \
	  Tank.cpp \
	  Berserk.cpp \
	  LanceMissile.cpp \
	  Display.cpp

OBJ	= $(SRC:.cpp=.o)

NAME	= ShootAndRun

CXXFLAGS	+= -I.sprites/

all: $(NAME)

$(NAME): $(OBJ)
	g++ -o $(NAME) $(OBJ) -lsfml-graphics -lsfml-window -lsfml-system

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all

.PHONY:all clean fclean re

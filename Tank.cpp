#include "Tank.hh"

Tank::Tank(int x, int y) : Unit(x, y)
{
  this->setHP(10);
  this->setAttack(5);
  this->setSpeed(7);
}

Tank::~Tank()
{
}

unitType	Tank::getType() const
{
  return (TANK);
}

void	Tank::restoreSpeed()
{
  this->setSpeed(7);
}

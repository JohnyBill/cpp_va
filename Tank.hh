#ifndef TANK_HH_
# define TANK_HH_

# include "Unit.hh"

class Tank : public Unit
{
public:
  Tank(int, int);
  ~Tank();

  void		restoreSpeed();
  unitType	getType() const;
};

#endif

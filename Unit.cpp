#include "Unit.hh"

Unit::Unit(int x, int y)
{
  this->_x = x;
  this->_y = y;
  this->_turn = false;
  this->_isAlive = true;
}

Unit::~Unit()
{}

int	Unit::getX() const
{
  return (this->_x);
}

int	Unit::getY() const
{
  return (this->_y);
}

void	Unit::setPosX(const int x)
{
  this->_x = x;
}

void	Unit::setPosY(const int y)
{
  this->_y = y;
}

void	Unit::setAttack(const int attack)
{
  this->_attack = attack;
}
int	Unit::getAttack() const
{
  return (this->_attack);
}

void	Unit::setSpeed(const int speed)
{
  this->_speed = speed;
}

int	Unit::getSpeed() const
{
  return (this->_speed);
}

int	Unit::getHP() const
{
  return (this->_hp);
}

void	Unit::setHP(const int hp)
{
  this->_hp = hp;
}

void	Unit::setTurn(bool turn)
{
  this->_turn = turn;
}

bool	Unit::getTurn() const
{
  return (this->_turn);
}

int	Unit::getTeam() const
{
  return (this->_team);
}

void	Unit::setTeam(int team)
{
  this->_team = team;
}

void	Unit::setAlive(bool alive)
{
  this->_isAlive = alive;
}

bool	Unit::isAlive() const
{
  return (this->_isAlive);
}

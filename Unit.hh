#ifndef UNIT_HH_
# define UNIT_HH_

enum unitType
  {
    BERSERK = 0,
    TANK = 1,
    LANCEMISSILE = 2
  };

class Unit
{
  int	_speed;
  int	_attack;
  int	_x, _y;
  int	_hp;
  bool	_turn;
  int	_team;
  bool	_isAlive;

public:
  Unit(int, int);
  ~Unit();

  void	setAlive(bool);
  bool	isAlive() const;
  void	setTeam(int);
  int	getTeam() const;
  bool	getTurn() const;
  void	setTurn(bool);
  int	getX() const;
  int	getY() const;
  void	setPosX(const int);
  void	setPosY(const int);
  void	setAttack(const int);
  int	getAttack() const;
  void	setSpeed(const int);
  int	getSpeed() const;
  int	getHP() const;
  void	setHP(const int);

  virtual unitType	getType() const = 0;
  virtual void		restoreSpeed() = 0;
};

#endif

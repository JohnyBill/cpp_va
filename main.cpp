#include <fstream>

#include "Game.hh"
#include "Tank.hh"

int	main(int ac, char **av)
{
  Game	*game = new Game();
  game->run();
  return (0);
}
